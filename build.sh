#!/bin/sh

set -e

bst track freedesktop-sdk-config.bst

bst track freedesktop-sdk.bst

bst build org.gnu.Hello.bst

rm -rf build
bst checkout org.gnu.Hello.bst build

flatpak build-export repo build stable
